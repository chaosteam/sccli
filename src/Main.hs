{-# LANGUAGE OverloadedStrings #-}
module Main where

import Control.Exception
import Control.Lens hiding ((<.>), argument)
import Control.Monad
import Control.Monad.Trans
import Data.Maybe
import Data.Monoid
import Filesystem
import Filesystem.Path.CurrentOS
import Options.Applicative
import Prelude hiding (FilePath, readFile, writeFile)
import Safe
import System.Exit
import Text.Regex.Applicative

import qualified Data.ByteString.Lazy as LBS
import qualified Data.Foldable as F
import qualified Data.Text as T
import qualified Data.Traversable as F

import API
import Browser hiding (header)
import Session

main :: IO ()
main = do
  credFile <- credentialsFile
  createTree (directory credFile)
  isFile credFile >>= flip unless (writeTextFile credFile "")
  let credFileName = join either T.unpack $ toText credFile
  creds <- readDefaultCredentials
  dir <- defaultDownloadDirectory
  args <- execParser $ argsParser credFileName dir creds
  getSession (user args) (password args) >>= maybe loginFailed
    (runBrowser (maxParallel args) $ app (downloadDir args) $ mode args)
 where
  loginFailed = putStrLn "Login failed. Please check your login credentials." >> exitFailure

app :: FilePath -> Mode -> Browser ()
app d (Sync cId) = sync cId d
app _ Clients  = clients
app _ (Upload file name) = upload file name
app d (Fetch url) = fetch d url
app _ (Activate cId) = activate cId

sync :: Int -> FilePath -> Browser ()
sync cId downloadDirectory = do
  team <- getDefaultTeam
  cls <- getClients team
  client <- cls `atMay` cId !!! "No such client"
  matches <- clientMatches <$> getClientInfo client
  F.for_ matches $ getMatchInfo >=> F.traverse_ (downloadMatch downloadDirectory team)

downloadMatch :: FilePath -> Team -> MatchInfo -> Browser ()
downloadMatch downloadDirectory self m = do
  liftIO $ putStrLn $ "Download | " ++ teamName enemy
  let dir = downloadDirectory </> fromText (T.pack (teamName enemy))
  liftIO $ createTree dir
  F.for_ (matchRounds m) $ downloadRound dir
  return ()
 where
   enemy :: Team
   enemy = case matchOpponents m of
    (a,b) | a /= self  -> a
          | otherwise -> b

downloadRound :: FilePath -> RoundInfo -> Browser ()
downloadRound dir roundInfo = do
  alreadyDownloaded <- liftIO $ isFile targetFile
  unless alreadyDownloaded $ do
    replay <- get (roundReplayUrl roundInfo)
    maybeLog <- F.traverse (postLink (roundReplayUrl roundInfo)) $
      roundFirstLogUrl roundInfo
    void $ liftIO $ writeFile targetFile (LBS.toStrict $ replay^.responseBody)
      `onException` removeFile targetFile
    void $ liftIO $ F.for maybeLog $ \r ->
      writeFile logFile (LBS.toStrict $ r^.responseBody)
        `onException` removeFile logFile
 where
  targetBasename, targetFile, logFile :: FilePath
  targetBasename = dir </> fromText (T.pack (show (roundId roundInfo) ++ "_" ++ scores))
  targetFile = targetBasename <.> "xml.gz"
  logFile = targetBasename <.> "log"
  scores = case roundInfo of
    RoundInfo t2 (TeamRoundInfo a _ (Just _)) _ -> showResult a (roundResult t2)
    RoundInfo t1 t2 _ -> showResult (roundResult t1) (roundResult t2)
  showResult r1 r2 = show (teamPoints r1) ++ d1 ++ "_" ++ show (teamPoints r2) ++ d2
   where
    d1 = if isJust (teamDisqualified r1) then "d" else ""
    d2 = if isJust (teamDisqualified r2) then "d" else ""

clients :: Browser ()
clients = do
  team <- getDefaultTeam
  clientsInfo <- getClients team >>= traverse getClientInfo
  liftIO $ putStrLn "Id │ Status │ Name"
  liftIO $ putStrLn "———│————————│——————"
  F.for_ (zip [0::Int ..] clientsInfo) $ \(n, clientInfo) ->
    liftIO $ putStrLn $ pad 2 ' ' (show n) ++ " │ " ++ maybe noStatus statusString (clientTestStatus clientInfo) ++ " │ " ++ clientName clientInfo
 where
  pad :: Int -> a -> [a] -> [a]
  pad n c ls = ls ++ replicate (n - length ls) c

  noStatus :: String
  noStatus = "------"

  statusString :: TestStatus -> String
  statusString TestSuccess = "  ok  "
  statusString TestFailure = " fail "
  statusString InProgress  = " .... "

upload :: FilePath -> String -> Browser ()
upload file name = do
  team <- getDefaultTeam
  archive <- liftIO $ readFile file
  client <- uploadClient team name $ LBS.fromStrict archive
  setClientMain client "client"
  testClient client False

(!!!) :: Maybe a -> String -> Browser a
Nothing !!! err = liftIO $ putStrLn ("Error: " ++ err) >> exitFailure
Just x  !!! _   = return x
infixl 0 !!!

fetch :: FilePath -> String -> Browser ()
fetch downloadDirectory url = do
  team <- getDefaultTeam
  url' <- match (few anySym *> optional (string "contest.software-challenge.de") *> many anySym) url !!! "Invalid match url"
  maybeMatchInfo <- getMatchInfo $ Match url'
  matchInfo <- maybeMatchInfo !!! "Match not finished yet"
  downloadMatch downloadDirectory team matchInfo

activate :: Int -> Browser ()
activate cId = do
  team <- getDefaultTeam
  cs <- getClients team
  c <- cs `atMay` cId !!! "No such client"
  activateClient c

--------------------------------------------------------------------------------
credentialsFile :: IO FilePath
credentialsFile = fmap (</> "credentials") $ getAppConfigDirectory "sccli"

readDefaultCredentials :: IO (Maybe String, Maybe String)
readDefaultCredentials = parse . lines . T.unpack <$> (readTextFile =<< credentialsFile)
 where
  parse :: [String] -> (Maybe String, Maybe String)
  parse ls = (ls ^? ix 0, ls ^? ix 1)

data Args = Args
  { user :: String
  , password :: String
  , downloadDir :: FilePath
  , maxParallel :: Integer
  , mode :: Mode
  }

data Mode = Sync Int
          | Clients
          | Upload FilePath String
          | Fetch String
          | Activate Int

defaultDownloadDirectory :: IO FilePath
defaultDownloadDirectory = fmap (</> "downloads") getWorkingDirectory

syncCmd :: ParserInfo Int
syncCmd = info (helper <*> argClientId) $
  fullDesc <> progDesc ("Sync all replays for the latest uploaded client. The replays will be stored in the download directory.")

clientsCmd :: ParserInfo ()
clientsCmd = void $ info helper $ fullDesc <> progDesc "Show all clients for your team."

uploadCmd :: ParserInfo Mode
uploadCmd = info (helper <*> parser) $ fullDesc <> progDesc "Upload a new client and set the main file. Also runs the tests and activates the client when the tests succeeded." where
  parser :: Parser Mode
  parser = Upload <$> fileArg <*> nameArg

  fileArg :: Parser FilePath
  fileArg = argument (fromText . T.pack <$> str) $ metavar "FILE"
    <> help "The file to upload. Should be a zip archive."

  nameArg :: Parser String
  nameArg = strOption $ short 'n' <> long "name" <> metavar "STRING" <> value ""
    <> help "The name for the client. Defaults to no name."

fetchCmd :: ParserInfo String
fetchCmd = info (helper <*> parser) $ fullDesc <> progDesc "Fetch a particular match. This match will be stored in the download directory, like all the synced matches." where
  parser :: Parser String
  parser = strArgument $ metavar "URL" <> help "URL of the match to fetch."

argClientId :: Parser Int
argClientId = argument auto $
  metavar "ID" <> help "ID of the client to activate, as shown by the clients subcommand"
  <> value 0

activateCmd :: ParserInfo Int
activateCmd = info (helper <*> argClientId) $
  fullDesc <> progDesc "Mark a client as the currently active client."

argsParser :: String -> FilePath -> (Maybe String, Maybe String) -> ParserInfo Args
argsParser credFileName defaultDir (defaultUser, defaultPassword) = info (helper <*> parser) $ header "CLI interface for the softwarechallenge contest system" where
  parser :: Parser Args
  parser = Args
    <$> userArg
    <*> passwordArg
    <*> downloadDirArg
    <*> maxParallelArg
    <*> subparser modes

  modes :: Mod CommandFields Mode
  modes = mconcat
    [ command "sync"     $ Sync     <$> syncCmd
    , command "clients"  $ Clients  <$ clientsCmd
    , command "upload"     uploadCmd
    , command "fetch"    $ Fetch    <$> fetchCmd
    , command "activate" $ Activate <$> activateCmd
    ]

  userArg :: Parser String
  userArg = strOption $ mconcat
    [ short 'u' <> long "user" <> metavar "USER"
    , showDefault <> F.foldMap value defaultUser
    , help $ "The username of the account to use for logging in. The first line of " <> credFileName <> " is used as a default for this option."
    ]

  passwordArg :: Parser String
  passwordArg = strOption $ mconcat
    [ short 'p' <> long "password" <> metavar "PASS"
    , showDefault <> F.foldMap value defaultPassword
    , help $ "The password to use. The second line of " <> credFileName <> " is used as a default for this option."
    ]

  downloadDirArg :: Parser FilePath
  downloadDirArg = option (fromText . T.pack <$> str) $ mconcat
    [ short 'd' <> long "dir" <> metavar "DIR"
    , value defaultDir
    , help $ "The download directory, where downloaded replays are stored. Defaults to ./download."
    ]

  maxParallelArg :: Parser Integer
  maxParallelArg = option auto $ mconcat
    [ short 'c' <> long "parallel" <> metavar "NUM"
    , value 10
    , help "The maximum number of tasks to execute in parallel (limits connections). "
    ]
