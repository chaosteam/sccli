{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE OverloadedStrings #-}
module API where

import Control.Lens
import Control.Monad
import Data.List
import Data.List.Split
import Data.Maybe
import Data.Time
import Network.HTTP.Client (RequestBody(..))
import Network.HTTP.Client.MultipartFormData
import Safe
import Text.Regex.Applicative
import Text.XML.Lens

import qualified Data.ByteString.Lazy as LBS
import qualified Data.Foldable as F
import qualified Data.Text as T
import qualified Data.Text.Lens as T

import Browser
import Util

-- | Construct an URL for this season
seasonUrl :: String -> String
seasonUrl = ("/saison/15" ++)

-- | Parse all links that have a href matching a given regex of an element.
readRegexLinks :: Read a => RE Char String -> (a -> String -> r) -> Fold Element r
readRegexLinks r f = deep (linkHrefRegex (fmap read r).to (uncurry f))

-- | Force a polymorphic value to be a list.
asList :: [a] -> [a]
asList = id

--------------------------------------------------------------------------------
data Team = Team { teamName :: String, teamId :: Integer } deriving (Eq, Ord, Show, Read)

teamUrl :: Team -> String -> String
teamUrl (Team _ i) url = seasonUrl $ "/teams/" ++ show i ++ url

-- | Return the list of teams the current user is a part of.
getTeams :: Browser [Team]
getTeams = get (seasonUrl "/meine-teams") <&> toListOf (responseBody.asXML.readRegexLinks teamRegex (flip Team)) where
  teamRegex :: RE Char String
  teamRegex = string (seasonUrl "/teams/") *> many anySym

-- | Get the first team for the logged in player. This assumes that players will only ever
-- be in one team.
getDefaultTeam :: Browser Team
getDefaultTeam = headNote "Not a member of any team" <$> getTeams

--------------------------------------------------------------------------------
data Client = Client
  { clientTeam :: Team
  , clientId :: Integer
  } deriving (Eq, Ord, Show, Read)

clientUrl :: Client -> String -> String
clientUrl (Client team i) url = teamUrl team $ "/computerspieler/" ++ show i ++ url

-- | Return the list of clients a given team uploaded.
getClients :: Team -> Browser [Client]
getClients team = get (teamUrl team "/computerspieler") <&> toListOf (responseBody.asXML.clientFromUrl team)

-- | Match the url to a client's details page from the overview page which lists all clients.
clientFromUrl :: Team -> Fold Element Client
clientFromUrl team = readRegexLinks (string (teamUrl team "/computerspieler/") *> few anySym <* "/client_details") (const . Client team)

-- | Upload a new client. Note that this will not set the default executable for the client or test the client.
uploadClient :: Team           -- ^ The team for which the client should be uploaded
             -> String         -- ^ The name of the client
             -> LBS.ByteString -- ^ Contents of the archive file to upload
             -> Browser Client -- ^ The client that was just uploaded
uploadClient team cname archive = do
  r <- post (teamUrl team "/computerspieler/new") (teamUrl team "/computerspieler")
    [ partFileRequestBody "client[file]" "client.zip" $ RequestBodyLBS archive
    , partString "client[name]" cname
    , partString "client[parameters]" ""
    , partString "client[new_vm]" "1"
    , partString "commit" ""
    ]
  return $ r ^?! responseBody.asXML.clientFromUrl team

-- | Set the main executable for the client to the file with the given name. This
-- assumes that that file exists. If the specified file does not exists, it will not
-- change anything.
setClientMain :: Client -> String -> Browser ()
setClientMain client main = do
  page <- get $ clientUrl client "/edit"
  let tds = page ^.. responseBody.asXML.deep (el "table".attributeIs "id" "fileList"...el "tr").to (toListOf $ plate.el "td")
      mains = map extractNameURL tds
  F.forM_ (lookup main mains) $ \url ->
    void $ postLink (clientUrl client "/edit") url
 where
  extractNameURL :: [Element] -> (String, String)
  extractNameURL tds = (cname, url) where
    cname :: String
    cname = fromJustNote "extractNameURL: missing name" $ tds ^? ix 1.text.to T.strip.T.unpacked

    url :: String
    url = fromJustNote "extractNameURL: missing url" $ tds ^? ix 3...el "a".attr "href".T.unpacked

-- | Test a client. This will only start testing the client but won't block till the tests are finished.
-- If you want to retrieve the test result, you should use 'getClientInfo'.
testClient :: Client     -- ^ The client to test
           -> Bool       -- ^ True if the client should be activated when the tests succeeded
           -> Browser ()
testClient client activate = void $ postForm (clientUrl client "") (clientUrl client "/test") ["activateClient" := showBool activate] where
  showBool :: Bool -> String
  showBool False = "false"
  showBool True = "true"

-- | Activate the client. This means it will be used for future matches.
activateClient :: Client -> Browser ()
activateClient client = void $ postLink (teamUrl (clientTeam client) "/computerspieler") (clientUrl client "/select")

-- | Hide the client. This will remove the client from the overview page, but it's still possible to access
-- the client details (also logs, replays) as long as you know the client's ID and team.
hideClient :: Client -> Browser ()
hideClient client = void $ postLink (teamUrl (clientTeam client) "/computerspieler") (clientUrl client "/hide")

--------------------------------------------------------------------------------
data ClientInfo = ClientInfo
  { clientName         :: String
  , clientTestStatus   :: Maybe TestStatus
  , clientMatches      :: [Match]
  , clientUploadDate   :: UTCTime
  }

data TestStatus = TestFailure | TestSuccess | InProgress

getClientInfo :: Client -> Browser ClientInfo
getClientInfo client = do
  (overviewPage, detailsPage) <- (,) <$> get (teamUrl (clientTeam client) "/computerspieler")
                                    <*> get (clientUrl client "/client_details")
  let detailsXml = detailsPage ^. responseBody.asXML
      overviewRow = overviewPage ^.. responseBody.asXML.getClientRow...el "td"
  return $ ClientInfo (getClientName overviewRow) (getClientStatus overviewRow) (getClientMatches detailsXml) (getClientUploadDate overviewRow)
 where
  getClientRow :: Fold Element Element
  getClientRow = deep $ el "tr" . filtered ((== Just client) . preview (clientFromUrl $ clientTeam client))

  getClientName :: [Element] -> String
  getClientName row = nameCell^.text.to T.strip.T.unpacked where
    nameCell :: Element
    nameCell = fromJustNote "getClientName: no name cell" $ row ^? ix 3

  getClientMatches :: Element -> [Match]
  getClientMatches = toListOf $ deep (el "tr" . failing (hasClass "matchday") (hasClass "friendly_encounter")) ... el "td" ... el "a" . attr "href" . T.unpacked . to normalize . to Match

  normalize :: String -> String
  normalize = fromMaybe <*> stripPrefix "http://contest.software-challenge.de"

  getClientStatus :: [Element] -> Maybe TestStatus
  getClientStatus row = row ^? ix 1...el "img".attr "alt".to readStatus where
    readStatus :: T.Text -> TestStatus
    readStatus "Ok" = TestSuccess
    readStatus "Spinner" = InProgress
    readStatus _ = TestFailure

  getClientUploadDate :: [Element] -> UTCTime
  getClientUploadDate row = fromJustNote "getClientUploadDate: invalid date" $
    parseTimeM True contestTimeLocale "%-d. %B %0Y" uploadDate
   where
    uploadInfo :: String
    uploadInfo = fromJustNote "getClientUploadDate: no upload info" $ row ^? ix 4...el "span".text.to T.strip.T.unpacked

    uploadDate :: String
    uploadDate = fromJustNote "getClientUploadDate: invalid info format" $ match (string "Am " *> few anySym <* string " von " <* many anySym) uploadInfo

contestTimeLocale :: TimeLocale
contestTimeLocale = defaultTimeLocale
  { months = zip monthNames (map snd $ months defaultTimeLocale)
  }
 where
  monthNames :: [String]
  monthNames = ["Januar", "Februar", "März", "April", "Mai", "Juni", "Juli", "August", "September", "Oktober", "November", "Dezember"]

--------------------------------------------------------------------------------
newtype Match = Match String deriving (Show, Eq, Ord, Read)
data MatchInfo = MatchInfo
  { matchOpponents :: (Team, Team)
  , matchRounds :: [RoundInfo]
  , matchResult :: (Integer, Integer)
  } deriving (Show, Read, Eq, Ord)

matchUrl :: Match -> String -> String
matchUrl (Match base) url = base ++ "/" ++ url

-- | Get the 'MatchInfo' for a given 'Match'.
getMatchInfo :: Match -> Browser (Maybe MatchInfo)
getMatchInfo m = do
  page <- get $ matchUrl m ""
  let rows = page ^.. responseBody.asXML.deep (el "table" . attributeIs "id" "rounds") ... el "tr"
      summary = (fromJustNote "getMatchInfo: no summary A" $ rows ^? ix 2, fromJustNote "getMatchInfo: no summary B" $ rows ^? ix 3)
      rounds = dropWhile (hasn't $ hasClass "round") rows
      pageTitle = page ^? responseBody.asXML.deep (el "head") ... el "title".text
  return $ if pageTitle == Just "Freundschaftsspiele"
    then Nothing
    else Just $ MatchInfo (getMatchOpponents summary) (getMatchRounds rounds) (getMatchResult summary)
 where
  getMatchOpponents :: (Element, Element) -> (Team, Team)
  getMatchOpponents = over both (fromJustNote "getMatchOpponents: no team link" . preview (elementOf (plate.el "td") 0 ... parseTeamLink))

  parseTeamLink :: Fold Element Team
  parseTeamLink = linkHrefRegex parser . to (uncurry $ flip Team) where
    parser :: RE Char Integer
    parser = fmap read $ few anySym *> string "teams/" *> some (psym (`elem` asList "0123456789")) <* many anySym

  getMatchRounds :: [Element] -> [RoundInfo]
  getMatchRounds = map (\(h:x:y:_) -> getRoundInfo h (x,y)) . split splitter where
    splitter :: Splitter Element
    splitter = dropInitBlank $ keepDelimsL $ whenElt (has $ hasClass "round")

  getRoundInfo :: Element -> (Element, Element) -> RoundInfo
  getRoundInfo h body = RoundInfo a b replayUrl where
    tabledata :: ([Element], [Element])
    tabledata = body & both %~ toListOf (plate . el "td")

    (a,b) = both %~ getTeamRoundInfo $ tabledata

    replayUrl :: String
    replayUrl = fromJustNote "getRoundInfo: no replay url" $ h ^? el "tr" ... el "td" ... el "div" ... el "span" ... el "a".attr "href".T.unpacked

  getMatchResult :: (Element, Element) -> (Integer, Integer)
  getMatchResult = over both (^?! elementOf (plate.el "td") 2.text.asRead)

  getTeamRoundInfo :: [Element] -> TeamRoundInfo
  getTeamRoundInfo tds = TeamRoundInfo result team logUrl where
    result :: TeamResult
    result = RoundResult (tds ^? ix 0 ... el "div".text.to T.strip.T.unpacked) (fromJustNote "getTeamRoundInfo: no points" $ tds ^? ix 2.text.to T.strip.asRead)

    team :: Team
    team = fromJustNote "getTeamRoundInfo: no team link" $ tds ^? ix 0...parseTeamLink

    logUrl :: Maybe String
    logUrl = tds ^? folded.hasClass "client_log" ... el "a".attr "href".T.unpacked

data TeamResult = RoundResult
  { teamDisqualified :: Maybe String
  , teamPoints :: Integer
  } deriving (Show, Read, Ord, Eq)

data TeamRoundInfo = TeamRoundInfo
  { roundResult :: TeamResult
  , roundTeam :: Team
  , roundLogUrl :: Maybe String
  } deriving (Show, Read, Ord, Eq)

data RoundInfo = RoundInfo TeamRoundInfo TeamRoundInfo String deriving (Show, Read, Ord, Eq)

roundReplayUrl :: RoundInfo -> String
roundReplayUrl (RoundInfo _ _ u) = u

roundId :: RoundInfo -> Integer
roundId = fromJustNote "roundId: Invalid replay" . match parser . roundReplayUrl where
  parser :: RE Char Integer
  parser = fmap read $ few anySym *> string "/rounds/" *> some anySym

roundFirstLogUrl :: RoundInfo -> Maybe String
roundFirstLogUrl (RoundInfo team1 team2 _) = roundLogUrl team1 <|> roundLogUrl team2

instance Field1 RoundInfo RoundInfo TeamRoundInfo TeamRoundInfo where
  _1 f (RoundInfo a b u) = f a <&> \a' -> RoundInfo a' b u

instance Field2 RoundInfo RoundInfo TeamRoundInfo TeamRoundInfo where
  _2 f (RoundInfo a b u) = f b <&> \b' -> RoundInfo a b' u
