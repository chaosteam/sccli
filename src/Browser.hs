{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE OverloadedStrings #-}
module Browser
  ( Browser()
  , runBrowser
  , get
  , post
  , postForm
  , postLink
  , module Network.Wreq
  ) where

import Control.Applicative
import Control.Concurrent.Async
import Control.Concurrent.MVar
import Control.Lens
import Control.Monad.Reader
import Network.HTTP.Client
import Network.Wreq hiding (get, post)

import qualified Data.ByteString.Lazy as LBS
import qualified Data.Text as T
import qualified Control.Concurrent.MSem as Sem
import qualified Data.Text.Encoding as T

import Prelude

import Session

newtype Browser a = Browser (ReaderT (Session, MVar (), Sem.MSem Integer) IO a)
  deriving (Functor, Monad)

instance Applicative Browser where
  pure = return
  Browser mf <*> Browser ma = Browser $ do
    conf <- ask
    (f, a) <- liftIO $ concurrently <$> runReaderT mf <*> runReaderT ma $ conf
    return $ f a

instance MonadReader Session Browser where
  ask = Browser $ view _1
  local f (Browser m) = Browser $ local (over _1 f) m

instance MonadIO Browser where
  liftIO io = Browser $ do
    lock <- view _2
    liftIO $ withMVar lock $ const io

-- | Perform an IO action that may run in parallel to the other actions. You should only
-- use this for things that don't have "real" side-effects, for example, for downloading a
-- file or fetching a webpage.
parallel :: IO a -> Browser a
parallel m = Browser $ do
  sem <- view _3
  liftIO $ Sem.with sem m

-- | Run the browser for a given session, spawning the given number of concurrent
-- workers for perfoming requests in parallel.
runBrowser :: Integer -> Browser a -> Session -> IO a
runBrowser factor (Browser m) session = do
  tasks <- Sem.new factor
  lock <- newMVar ()
  runReaderT m (session, lock, tasks)
    
-- | Perform a GET request to the contest system.
get :: String -> Browser (Response LBS.ByteString)
get url = do
  session <- ask
  parallel $ getWith (disableTimeout $ defaults & cookies ?~ authCookies session) $ baseUrl url

-- | Perform a POST request to the contest system.
post :: String  -- ^ Referer URL
     -> String  -- ^ URL to post to.
     -> [Part]  -- ^ Form field data.
     -> Browser (Response LBS.ByteString)
post referer url ps = do
  session <- ask
  let opts = disableTimeout $ defaults
           & cookies ?~ authCookies session
           & header "Referer" .~ [T.encodeUtf8 $ T.pack $ baseUrl referer]
  parallel $ postWith opts (baseUrl url) $ partString "authenticity_token" (authToken session) : ps

-- We don't really need the following function, as all forms on the contest system
-- allow us to use post instead of postForm. But 'post' doesn't work with unpatched HTTP
-- (need to clear the Content-Type on redirect), so using 'postForm' allows at least
-- some functions to work even when using an unpatched version of HTTP.
postForm :: String       -- ^ Referer URL
         -> String       -- ^ URL to post to.
         -> [FormParam]  -- ^ Form field data.
         -> Browser (Response LBS.ByteString)
postForm referer url ps = do
  session <- ask
  let opts = disableTimeout $ defaults
           & cookies ?~ authCookies session
           & header "Referer" .~ [T.encodeUtf8 $ T.pack $ baseUrl referer]
  parallel $ postWith opts (baseUrl url) $ ("authenticity_token" := authToken session) : ps

-- | Set the timeout to None for the given options.
disableTimeout :: Options -> Options
disableTimeout = set manager $ Left $ defaultManagerSettings { managerResponseTimeout = Nothing }

-- | Click on a link via a post request. This is needed quite often for
-- the contest system, because many links require the CSRF token, which must
-- be sent via a post request.
postLink :: String       -- ^ Referer URL
         -> String       -- ^ URL to post to.
         -> Browser (Response LBS.ByteString)
postLink referer url = postForm referer url []

-- | Prefix the base "http://contest.software-challenge.de" to the url
baseUrl :: String -> String
baseUrl = ("http://contest.software-challenge.de" ++)
