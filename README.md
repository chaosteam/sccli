sccli [![wercker status](https://app.wercker.com/status/96d926606ca1e1e9e738014e117d9ec6/s "wercker status")](https://app.wercker.com/project/bykey/96d926606ca1e1e9e738014e117d9ec6)
====================

Command line interface to the SoftwareChallenge contest website.
